<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<!-- Título de categoría -->
<h2><?php single_cat_title(); ?></h2>
<!-- Autor -->
<p>Posts de <strong><?php echo get_the_author(); ?></strong></p>
<!-- Listado de posts -->
<?php if ( have_posts() ) : ?>
<?php get_footer(); ?>