<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
    <link rel="stylesheet" href="plugins/bootstrap-3.3.7-dist/css/bootstrap.css" />
	
    <script type="text/javascript" src="plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
    <?php wp_head(); ?>
  </head>
  <body>
    <header>
      <h1><?php bloginfo('name'); ?></h1>
      <?php the_category (); ?>
    </header>
    <!-- creacion del menu principal -->
    <nav>
      <ul class="main-nav">
        <?php wp_nav_menu( array( 'theme_location' => 'navegation' ) ); ?>
      </ul>
    </nav>